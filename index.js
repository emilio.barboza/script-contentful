const { createClient } = require('contentful-management');
const axios = require('axios');

const client = createClient({
	// This is the access token for this space. Normally you get the token in the Contentful web app
	accessToken: 'CFPAT-k2DYaGTnCwqIFx61c7S0pWDmU5MqLINqs7g0j8JJpbI',
});
const opcContentful = {
	spaceId: 'opsxcscyfqjk',
	env: 'master',
	contentModel: 'testModel',
};

const structImgData = (product) => ({
	fields: {
		title: {
			'en-US': product.title,
		},
		description: {
			'en-US': product.description,
		},
		file: {
			'en-US': {
				contentType: 'image/png',
				fileName: `${product.title}.png`,
				upload: product.image,
			},
		},
	},
});

const test = async () => {
	const req = await axios.get('https://fakestoreapi.com/products');
	console.log(req.data.length);
	for (let product of req.data) {
		const newData = {
			fields: {
				id: { 'en-US': product.id },
				title: { 'en-US': product.title },
				price: { 'en-US': product.price },
				description: { 'en-US': product.description },
				category: { 'en-US': product.category },
				image: { 'en-US': {} },
				rating: { 'en-US': 5 }, // random number 1 to 5
				discount: { 'en-US': 10 }, // random number 0 to 10
				feature: { 'en-US': '' },
			},
		};
		const space = await client.getSpace(opcContentful.spaceId);
		const env = await space.getEnvironment(opcContentful.env);
		const img = structImgData(product);
		const asset = await env.createAsset(img);
		const auxImg = await asset.processForAllLocales();
		await auxImg.publish();
		console.log(asset.sys.id);
		newData.fields.image = {
			'en-US': {
				sys: { type: 'Link', linkType: 'Asset', id: asset.sys.id },
			},
		};
		let entry;
		const res = await env.createEntry(opcContentful.contentModel, newData);
		entry = await env.getEntry(res.sys.id);
		await entry.publish();
		console.log(entry.fields);
	}
};

test();
